﻿using System;
using Xamarin.Forms;
using XUI.Globals;

namespace XUI.Components
{
    public class Wrapper : Frame
    {
        public TypeOfWrapper XUIWrapperType
        {
            get
            {
                return (TypeOfWrapper)GetValue(XUIWrapperTypeProperty);
            }
            set
            {
                SetValue(XUIWrapperTypeProperty, value);
            }
        }

        public static readonly BindableProperty XUIWrapperTypeProperty = BindableProperty.Create(
                                                                   "XUIWrapperType",
                                                                   returnType: typeof(TypeOfWrapper),
                                                                   declaringType: typeof(Wrapper),
                                                                   defaultValue: TypeOfWrapper.Default);


        private void ChangeWrapperType()
        {
            switch (XUIWrapperType)
            {
                case TypeOfWrapper.Radius:
                    RadiusWrapperSelected();
                    break;
                case TypeOfWrapper.Circular:
                    CircularWrapperSelected();
                    break;
                default:
                    this.Content = XUIWrapperContent;
                    break;
            }
        }


        public static readonly BindableProperty XUIWrapperContentProperty = BindableProperty.Create(
                                                           "XUIWrapperContent",
                                                           returnType: typeof(Layout),
                                                           declaringType: typeof(Wrapper),
                                                           defaultValue: default(Layout),
                                                           propertyChanged: ChangeWrapperContent);

        private static void ChangeWrapperContent(BindableObject bindable, object oldValue, object newValue)
        {
            if (oldValue == newValue) return;
            ((Wrapper)bindable).ChangeWrapperType();
        }

        public Layout XUIWrapperContent
        {
            get
            {
                return (Layout)GetValue(XUIWrapperContentProperty);
            }
            set
            {
                SetValue(XUIWrapperContentProperty, value);
            }
        }

        public double XUISingleSize
        {
            get
            {
                return (double)GetValue(XUISingleSizeProperty);
            }
            set
            {
                SetValue(XUISingleSizeProperty, value);
            }
        }

        public static readonly BindableProperty XUISingleSizeProperty = BindableProperty.Create(
                                                                    "XUISingleSize",
                                                                    returnType: typeof(double),
                                                                    declaringType: typeof(Wrapper),
                                                                    defaultValue: default(double),
                                                                    propertyChanged: ChangeSingleSize);

        private static void ChangeSingleSize(BindableObject bindable, object oldValue, object newValue)
        {
            if (oldValue == newValue) return;
            ((Wrapper)bindable).ChangeSingleSize();
        }

        private void ChangeSingleSize()
        {
            this.HeightRequest = this.WidthRequest = XUISingleSize;
        }

        private void CircularWrapperSelected()
        {
            if (XUISingleSize == default(double))
            {
                throw new ArgumentNullException("XUISingleSize", "In order to use a circular wrapper, you will need to complete the XUISingleSize property");
            }
            this.HorizontalOptions = LayoutOptions.Center;
            this.VerticalOptions = LayoutOptions.Center;
            this.CornerRadius = (int)XUISingleSize / 2;
            this.Content = XUIWrapperContent;
        }

        private void RadiusWrapperSelected()
        {
            this.XUIWrapperContent.HorizontalOptions = LayoutOptions.Fill;
            this.XUIWrapperContent.VerticalOptions = LayoutOptions.Fill;
            this.Content = XUIWrapperContent;

            switch (Device.RuntimePlatform)
            {
                case Device.iOS:
                    this.CornerRadius = 15;
                    break;
                case Device.Android:
                    this.CornerRadius = 20;
                    break;
                default:
                    break;
            }
        }

        public Wrapper()
        {
            this.Margin = 0;
            this.Padding = 0;
            this.OutlineColor = Color.Transparent;
            this.HasShadow = false;
        }
    }
}
