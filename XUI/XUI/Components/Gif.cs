﻿using Xamarin.Forms;

namespace XUI.Components
{
    public class Gif : WebView
    {
        public static readonly BindableProperty XUIGifProperty = BindableProperty.Create(
                                                           "XUIGif",
                                                           returnType: typeof(string),
                                                           declaringType: typeof(Gif),
                                                           defaultValue: default(string),
                                                           propertyChanged: ChangeGif);

        public string XUIGif
        {
            get
            {
                return (string)GetValue(XUIGifProperty);
            }
            set
            {
                SetValue(XUIGifProperty, value);
            }
        }

        private static void ChangeGif(BindableObject bindable, object oldValue, object newValue)
        {
            if (oldValue == newValue) return;
            ((Gif)bindable).ChangeGif();
        }

        public void ChangeGif()
        {

            this.Source = new HtmlWebViewSource { Html = $"<html><body><img src='{ XUIGif }' style='width:100%; height:100%' /></body></html>" };
        }


    }
}
