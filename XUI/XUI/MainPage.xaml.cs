﻿using Xamarin.Forms;
using XUI.ViewModels;

namespace XUI
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            this.BindingContext = new MainPageViewModel();
        }
    }
}
