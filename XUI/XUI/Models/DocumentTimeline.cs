﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XUI.Models
{
    public class DocumentTimeline
    {
        private string text;
        public string Text
        {
            get { return text; }
            set { text = value; }
        }

        private string personName;
        public string PersonName
        {
            get { return personName; }
            set { personName = value; }
        }

        private DateTime eventDateTime;
        public DateTime EventDateTime
        {
            get { return eventDateTime; }
            set { eventDateTime = value;}
        }

        private bool appearanceFlag;
        public bool AppearanceFlag
        {
            get { return appearanceFlag; }
            set { appearanceFlag = value; }
        }


        private string icon;
        public string Icon
        {
            get { return icon; }
            set { icon = value; }
        }
    }
}
