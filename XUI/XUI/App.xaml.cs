﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace XUI
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            //MainPage = new XUI.Views.Social.SocialProfile();
            //MainPage = new XUI.Components.Page1();
            MainPage = new XUI.Views.Dashboards.MultiTitleDashboard();

        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
