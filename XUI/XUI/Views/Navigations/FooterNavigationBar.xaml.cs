﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XUI.Views.Navigations
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FooterNavigationBar : ContentPage
	{
		public FooterNavigationBar ()
		{
			InitializeComponent ();
		}
	}
}