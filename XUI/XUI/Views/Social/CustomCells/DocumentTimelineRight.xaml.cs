﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XUI.Views.Social.CustomCells
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DocumentTimelineRight : ViewCell
    {
        public DocumentTimelineRight()
        {
            InitializeComponent();
        }
    }
}