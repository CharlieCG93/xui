﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XUI.Views.Social
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SocialProfile : ContentPage
	{
		public SocialProfile ()
		{
			InitializeComponent ();
            ContactList.ItemsSource = new List<Object> {
                new { Name = "Richard Stallman", Picture = "http://i.nextmedia.com.au/news/richard-stallman.jpg" },
                new { Name = "Linus Torvalds", Picture = "https://avatars3.githubusercontent.com/u/1024025?v=4&s=460"  },
                new { Name = "Andrew Tanenbaum", Picture = "http://www.azquotes.com/public/pictures/authors/2a/32/2a3237530b8b85d0f23d0767bc990fc8/54e7d67f56320_andrew_s_tanenbaum.jpg"  },
                new { Name = "Eric Raymond",Picture = "http://greatthoughtstreasury.com/sites/default/files/eric-s-raymond_x270%5B1%5D.jpg"  },
                new { Name = "Miguel De Icaza", Picture = "http://www.itwriting.com/blog/wp-content/uploads/2013/06/image8.png"  }
            };

        }
	}
}