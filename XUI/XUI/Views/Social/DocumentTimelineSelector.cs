﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace XUI.Views.Social { 
    public class DocumentTimelineSelector : Xamarin.Forms.DataTemplateSelector
    {
        private readonly DataTemplate documentTimelineLeft;
        private readonly DataTemplate documentTimelineRight;

        public DocumentTimelineSelector()
        {
            this.documentTimelineLeft = new DataTemplate(typeof(CustomCells.DocumentTimelineLeft));
            this.documentTimelineRight = new DataTemplate(typeof(CustomCells.DocumentTimelineRight));
        }
        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            var row = item as Models.DocumentTimeline;
            if (row == null)
                return null;
            return row.AppearanceFlag ? this.documentTimelineRight : this.documentTimelineLeft;
        }
    }
}
