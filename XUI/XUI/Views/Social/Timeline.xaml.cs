﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XUI.Views.Social
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Timeline : ContentPage
    {
        public Timeline()
        {
            InitializeComponent();
            TimelineList.ItemsSource = new List<Object> {
                new { Name = "Richard Stallman", Picture = "http://i.nextmedia.com.au/news/richard-stallman.jpg", Description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", Date="JUN 21 2016" },
                new { Name = "Linus Torvalds", Picture = "https://avatars3.githubusercontent.com/u/1024025?v=4&s=460", Description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", Date="JUL 2 2016"},
                new { Name = "Andrew Tanenbaum", Picture = "http://www.azquotes.com/public/pictures/authors/2a/32/2a3237530b8b85d0f23d0767bc990fc8/54e7d67f56320_andrew_s_tanenbaum.jpg", Description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", Date="SEP 15 2016" },
                new { Name = "Eric Raymond",Picture = "http://greatthoughtstreasury.com/sites/default/files/eric-s-raymond_x270%5B1%5D.jpg" , Description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", Date="SEP 17 2016"   },
                new { Name = "Miguel De Icaza", Picture = "http://www.itwriting.com/blog/wp-content/uploads/2013/06/image8.png", Description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", Date="OCT 31 2016"    },
                new { Name = "Richard Stallman", Picture = "http://i.nextmedia.com.au/news/richard-stallman.jpg", Description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", Date="DIC 25 2016"  },
                new { Name = "Linus Torvalds", Picture = "https://avatars3.githubusercontent.com/u/1024025?v=4&s=460", Description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", Date="ENE 2 2017"  },
                new { Name = "Andrew Tanenbaum", Picture = "http://www.azquotes.com/public/pictures/authors/2a/32/2a3237530b8b85d0f23d0767bc990fc8/54e7d67f56320_andrew_s_tanenbaum.jpg", Description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", Date="FEB 15 2017"  },
                new { Name = "Eric Raymond",Picture = "http://greatthoughtstreasury.com/sites/default/files/eric-s-raymond_x270%5B1%5D.jpg" , Description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", Date="ABR 21 2017"  },
                new { Name = "Miguel De Icaza", Picture = "http://www.itwriting.com/blog/wp-content/uploads/2013/06/image8.png", Description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", Date="MAY 15 2017"  }
            };
        }
    }
}