﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XUI.Views.Social
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DocumentTimeline : ContentPage
    {
        public DocumentTimeline()
        {
            InitializeComponent();
            var content = new ViewModels.DocumentTimelineViewModel();
            this.DocumentTimeLine.ItemsSource = content.DocumentEdition;
        }
    }
}