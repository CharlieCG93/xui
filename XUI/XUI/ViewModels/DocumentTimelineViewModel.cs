﻿using System;
using System.Collections.ObjectModel;
using XUI.Models;

namespace XUI.ViewModels
{
    public class DocumentTimelineViewModel : ViewModelBase
    {
        public ObservableCollection<DocumentTimeline> DocumentEdition { get; set; }
        public DocumentTimelineViewModel()
        {
            DocumentEdition = new ObservableCollection<DocumentTimeline>
            {
                new DocumentTimeline { Text = "Hello World", AppearanceFlag = true, EventDateTime = DateTime.Now.AddMinutes(-25), Icon = "asd", PersonName="Charlie"},
                new DocumentTimeline { Text = "Hello World", AppearanceFlag = false, EventDateTime = DateTime.Now.AddMinutes(-25), Icon = "asd", PersonName="Charlie"},
                new DocumentTimeline { Text = "Hello World", AppearanceFlag = true, EventDateTime = DateTime.Now.AddMinutes(-25), Icon = "asd", PersonName="Charlie"},
                new DocumentTimeline { Text = "Hello World", AppearanceFlag = false, EventDateTime = DateTime.Now.AddMinutes(-25), Icon = "asd", PersonName="Charlie"},
                new DocumentTimeline { Text = "Hello World", AppearanceFlag = true, EventDateTime = DateTime.Now.AddMinutes(-25), Icon = "asd", PersonName="Charlie"},
            };
        }

    }
}
