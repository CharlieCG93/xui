﻿using Xamarin.Forms;

namespace XUI.ViewModels
{
    class MainPageViewModel
    {
        public Command SettingsCommand { get; set; }

        public MainPageViewModel()
        {
            this.SettingsCommand = new Command(async () => {
                await App.Current.MainPage.DisplayAlert("Test", "Aproved", "OK");
            });
        }
    }
}
